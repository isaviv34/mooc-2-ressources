-   Objectifs

  Découverte du langage Python, prise en main d'un interpréteur Python, création de figures géométriques
-   Pré-requis à cette activité

  Connaître le nom des principaux polygones ; Connaître l'unité graphique de base (pixel) ; Connaître le principe de boucle algorithmique ; 
-   Durée de l'activité

Trois heures
  
-   Exercices cibles
  
-   Description du déroulement de l'activité
  
-   Anticipation des difficultés des élèves

-   Gestion de l'hétérogénéïté
